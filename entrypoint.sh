#!/bin/bash

set -euo pipefail

set -x

LABELS="${LABELS:-}"
NAME="${NAME:-}"

cleanup() {
    echo "Removing runner..."
    ./config.sh remove --unattended --token "${TOKEN}"
}

EXTRA_ARGS=""
if [ -n "${LABELS}" ]; then
    EXTRA_ARGS="${EXTRA_ARGS} --labels ${LABELS}"
fi

if [ -n "${NAME}" ]; then
    EXTRA_ARGS="${EXTRA_ARGS} --name ${NAME}"
fi

./config.sh \
    --url "https://github.com/contino/${REPOSITORY}" \
    --token "${TOKEN}" "${EXTRA_ARGS}" \
    --unattended

trap "cleanup" INT TERM

./run.sh
