FROM ubuntu:20.04

ARG RUNNER_VERSION="2.314.1"
ENV TOKEN=""
ENV REPOSITORY=""

# Automatic ARG variables
ARG TARGETARCH

RUN useradd -m docker
RUN apt update && apt install -y ca-certificates

RUN mkdir -p /home/docker/actions-runner

ADD https://github.com/actions/runner/releases/download/v${RUNNER_VERSION}/actions-runner-linux-${TARGETARCH}-${RUNNER_VERSION}.tar.gz .
RUN tar -C /home/docker/actions-runner -xzf actions-runner-linux-${TARGETARCH}-${RUNNER_VERSION}.tar.gz
RUN chmod +x /home/docker/actions-runner/bin/installdependencies.sh
RUN /home/docker/actions-runner/bin/installdependencies.sh

# at the bottom so caching applies to the heavy lifters above
COPY ./entrypoint.sh /home/docker/actions-runner/entrypoint.sh

RUN chown -R docker /home/docker

USER docker
WORKDIR /home/docker/actions-runner
ENTRYPOINT ["./entrypoint.sh"]
